/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.softdev.oxnextgentest;

/**
 *
 * @author slmfr
 */
public class OxNextGenTest {
    
    static boolean IsWin(char[][] board,char turn){
        
        return IsRowWin(board, turn) || IsColWin(board, turn) || IsDiagonalsWin(board, turn);
    }
    
    static boolean IsRowWin(char[][] board,char turn){
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == turn && board[row][1] == turn && board[row][2] == turn) {
                return true;
            }
        }
        return false;
    }
    
    static boolean IsColWin(char[][] board,char turn){
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == turn && board[1][col] == turn && board[2][col] == turn) {
                return true;
            }
        }
        return  false;
    }
    
    static boolean IsDiagonalsWin(char[][] board,char turn){
                
        return (board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) || (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn);
    }
    
    static boolean IsBoardFull(char[][] board){
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
