/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.softdev.oxnextgentest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author slmfr
 */
public class OxUnitTest {
    
    public OxUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckWinIsFalse() {
        char turn = 'X';
        char[][] board = {{'X','X','O'},{'X','O','-'},{'-','-','-'}};
        assertEquals(false,OxNextGenTest.IsWin(board, turn));
    }
    
    @Test
    public void testCheckWinIsTrue() {
        char turn = 'X';
        char[][] board = {{'X','X','X'},{'O','O','-'},{'-','-','-'}};
        assertEquals(true,OxNextGenTest.IsWin(board, turn));
    }
    
//    --------------------------------------------------------------
    @Test
    public void testCheckIsRowWinByFalse(){
        char turn = 'X';
        char[][] board = {{'X','X','O'},{'X','O','-'},{'-','-','-'}};
        assertEquals(false, OxNextGenTest.IsRowWin(board, turn));
    }
    
    @Test
    public void testCheckIsRowWinByTrue(){
        char turn = 'X';
        char[][] board = {{'X','X','X'},{'O','O','-'},{'-','-','-'}};
        assertEquals(true, OxNextGenTest.IsRowWin(board, turn));
    }
    
    @Test
    public void testCheckIsRowWinByFalseButNotMyTurn(){
        char turn = 'O';
        char[][] board = {{'X','X','X'},{'O','O','-'},{'-','-','-'}};
        assertEquals(false, OxNextGenTest.IsRowWin(board, turn));
    }
    
//    ---------------------------------------------------------------
    @Test
    public void testCheckIsColWinByFalse(){
        char turn = 'O';
        char[][] board = {{'X','X','X'},{'O','O','-'},{'-','-','-'}};
        assertEquals(false, OxNextGenTest.IsColWin(board, turn));
    }
    
    @Test
    public void testCheckIsColWinByTrue(){
        char turn = 'X';
        char[][] board = {{'X','-','O'},{'X','O','-'},{'X','-','-'}};
        assertEquals(true, OxNextGenTest.IsColWin(board, turn));
    }
    
    @Test
    public void testCheckIsColWinByFalseButNotMyTurn(){
        char turn = 'O';
        char[][] board = {{'X','-','O'},{'X','O','-'},{'X','-','-'}};
        assertEquals(false, OxNextGenTest.IsColWin(board, turn));
    }
//    ---------------------------------------------------------------
    @Test
    public void testCheckIsDiagonalsWinByFalse(){
        char turn = 'X';
        char[][] board = {{'X','-','O'},{'X','O','-'},{'X','-','-'}};
        assertEquals(false, OxNextGenTest.IsDiagonalsWin(board, turn));
    }
    
    @Test
    public void testCheckIsDiagonalsWinByTrue(){
        char turn = 'X';
        char[][] board = {{'X','-','O'},{'O','X','-'},{'-','-','X'}};
        assertEquals(true, OxNextGenTest.IsDiagonalsWin(board, turn));
    }
    
    @Test
    public void testCheckIsDiagonalsWinByFalseButNotMyTurn(){
        char turn = 'O';
        char[][] board = {{'X','-','O'},{'O','X','-'},{'-','-','X'}};
        assertEquals(false, OxNextGenTest.IsDiagonalsWin(board, turn));
    }
//   ------------------------------------------------------------------
    
    @Test
    public void testCheckIsBoardFullByFalse(){
        char[][] board = {{'X','-','O'},{'O','X','-'},{'-','-','X'}};
        assertEquals(false, OxNextGenTest.IsBoardFull(board));
    }
    
    @Test
    public void testCheckIsBoardFullByTrue(){
        char[][] board = {{'X','X','O'},{'O','X','O'},{'X','O','X'}};
        assertEquals(true, OxNextGenTest.IsBoardFull(board));
    }
    
}
